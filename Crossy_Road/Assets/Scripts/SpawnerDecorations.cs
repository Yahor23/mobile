﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class SpawnerDecorations : MonoBehaviour
{
    public Transform startPos = null;
    public GameObject player = null;
    public SpawnController spawnController = null;
    public GameObject groundGenerator = null;

    public float limit = 5f;

    public int spawnCountMin = 4;
    public int spawnCountMax = 20;
    public int distanceSpawn = 18;

    private bool isChecked = false;
    private List<GameObject> items = new List<GameObject>();
    [HideInInspector] public string poolName;
    [HideInInspector] public bool goLeft = false;
    [HideInInspector] public Vector3 spawnLeftPos;
    [HideInInspector] public float spawnRightPos = 0;
    void Start()
    {
        groundGenerator = GameObject.Find("GroundGenerator");
        player = GameObject.Find("Player");
        spawnLeftPos.x = Mathf.Round(spawnLeftPos.x);

            int spawnCount = Random.Range(spawnCountMin, spawnCountMax);
            SpawnDecoration(spawnCount, spawnLeftPos);

    }

    void Update()
    {
        if (!isChecked) { CheckPosition(); }
    }

    void SpawnDecoration(int spawnCount, Vector3 pos)
    {
        if (spawnCount == 0) { return; }
        float increment;
        if (goLeft) { increment = -1; }
        else { increment = 1; }
        int randomValue;
        for (int i = 0; i < distanceSpawn; ++i)
        {
            GameObject go;
            pos.x += increment;
            randomValue = Random.Range(1, 100);
            if (randomValue % 5 == 0)
            {
                if (poolName == "TreeSmall" || poolName == "TreeLarge")
                {
                    go = EasyObjectPool.instance.GetObjectFromPool(poolName, pos, Quaternion.Euler(-90, 0, 0));
                    items.Add(go);
                }
                else
                {
                    go = EasyObjectPool.instance.GetObjectFromPool(poolName, pos, Quaternion.Euler(0, 0, 0));
                    items.Add(go);
                }
                --spawnCount;
            }
            if (spawnCount == 0) { return; }
        }
    }

    void CheckPosition()
    {
        float different = player.transform.position.z - spawnLeftPos.z;
        if (different > limit)
        {
            for (int i = 0; i < items.Count; ++i)
            {
                EasyObjectPool.instance.ReturnObjectToPool(items[i]);
            }
            items.Clear();
            isChecked = true;
        }
    }
    public void Respawn()
    {
        spawnLeftPos.x = Mathf.Round(spawnLeftPos.x);
        int spawnCount = Random.Range(spawnCountMin, spawnCountMax);
        SpawnDecoration(spawnCount, spawnLeftPos);
        isChecked = false;
    }
}
