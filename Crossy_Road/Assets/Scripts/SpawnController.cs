﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public bool goLeft = false;
    public bool goRight = false;

    public List<string> decorationList = new List<string>();
    public List<Spawner> spawnersLeft = new List<Spawner>();
    public List<Spawner> spawnersRight = new List<Spawner>();
    public List<SpawnerDecorations> spawnersDecorationsRight = new List<SpawnerDecorations>();
    public List<SpawnerDecorations> spawnersDecorationsLeft = new List<SpawnerDecorations>();

    private bool started = false;
    public bool isReady = false;

    void Start()
    {
        started = true;
        int itemId1 = Random.Range(0, decorationList.Count);
        int itemId2 = Random.Range(0, decorationList.Count);

        string firstPoolName = decorationList[itemId1];
        string secondPoolName = decorationList[itemId2];

        int direction = Random.Range(0, 2);

        if (direction > 0) { goLeft = false; goRight = true; } else { goLeft = true; goRight = false; }
        if (spawnersLeft.Count > 0)
        {
            for (int i = 0; i < spawnersLeft.Count; i++)
            {
                if (i % 2 != 0)
                {
                    spawnersLeft[i].poolName = firstPoolName;

                }
                else
                {
                    spawnersLeft[i].poolName = secondPoolName;

                }
                spawnersLeft[i].goLeft = goLeft;
                spawnersLeft[i].gameObject.SetActive(goRight);
                spawnersLeft[i].spawnLeftPos = spawnersLeft[i].transform.position;
            }
        }
        else
        {
            for (int i = 0; i < spawnersDecorationsLeft.Count; i++)
            {
                if (i % 2 != 0)
                {
                    spawnersDecorationsLeft[i].poolName = firstPoolName;

                }
                else
                {
                    spawnersDecorationsLeft[i].poolName = secondPoolName;

                }
                spawnersDecorationsLeft[i].goLeft = goLeft;
                spawnersDecorationsLeft[i].gameObject.SetActive(goRight);
                spawnersDecorationsLeft[i].spawnLeftPos = spawnersDecorationsLeft[i].transform.position;
            }
        }

            if (spawnersRight.Count > 0)
            {
                for (int i = 0; i < spawnersRight.Count; i++)
                {
                    if (i % 2 != 0)
                    {
                        spawnersRight[i].poolName = firstPoolName;

                    }
                    else
                    {
                        spawnersRight[i].poolName = secondPoolName;

                    }
                    spawnersRight[i].goLeft = goLeft;
                    spawnersRight[i].gameObject.SetActive(goLeft);
                    spawnersRight[i].spawnLeftPos = spawnersRight[i].transform.position;
                }
            }
            else
            {
                for (int i = 0; i < spawnersDecorationsRight.Count; i++)
                {
                    if (i % 2 != 0)
                    {
                        spawnersDecorationsRight[i].poolName = firstPoolName;

                    }
                    else
                    {
                        spawnersDecorationsRight[i].poolName = secondPoolName;

                    }
                spawnersDecorationsRight[i].goLeft = goLeft;
                    spawnersDecorationsRight[i].gameObject.SetActive(goLeft);
                    spawnersDecorationsRight[i].spawnLeftPos = spawnersDecorationsRight[i].transform.position;
                }
            }
        
    }
        void Update()
        {
        if (isReady)
        {
            ChangePosition();
            isReady = false;
        }
        }
    public void ChangePosition()
    {
        if (started)
        {
            for (int i = 0; i < spawnersDecorationsLeft.Count; i++)
            {
                spawnersDecorationsLeft[i].spawnLeftPos = spawnersDecorationsLeft[i].transform.position;
                if (goRight)
                    spawnersDecorationsLeft[i].Respawn();
            }
            for (int i = 0; i < spawnersDecorationsRight.Count; i++)
            {
               spawnersDecorationsRight[i].spawnLeftPos = spawnersDecorationsRight[i].transform.position;
                if (goLeft)
                    spawnersDecorationsRight[i].Respawn();
            }
            for (int i = 0; i < spawnersLeft.Count; i++)
            {
                spawnersLeft[i].spawnLeftPos = spawnersLeft[i].transform.position;
                if (goRight)
                    spawnersLeft[i].SetParameters();
            }
            for (int i = 0; i < spawnersRight.Count; i++)
            {                spawnersRight[i].spawnLeftPos = spawnersRight[i].transform.position;
                if (goLeft)
                    spawnersRight[i].SetParameters();
            }
        }
        else return;
    }
}
