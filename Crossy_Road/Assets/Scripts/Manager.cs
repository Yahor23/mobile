﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MarchingBytes;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using System;

public class Manager : MonoBehaviour
{
    public AudioMixerGroup Mixer;
    public PlayerController player;
    public int levelCount = 50;
    public Text coin = null;
    public Text distance = null;
    public Text recordText = null;
    public Camera camera = null;
    public GroundGenerator groundGenerator = null;

    private GameObject ballGameObject;
    private float rotation;
    private Vector3 direction;
    private int recordInt;
    private int currentCoins;
    private int currentDistance = 0;
    private bool canPlay = false;
    private bool canFollow = false;

    private static Manager s_Instance;
    public static Manager instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = FindObjectOfType(typeof(Manager)) as Manager;
            }

            return s_Instance;
        }
    }
    void Start()
    {
        CheckMusic();
        CheckGodMode();
        recordInt = PlayerPrefs.GetInt("Record");
        currentCoins = PlayerPrefs.GetInt("Coin");
        coin.text = currentCoins.ToString();
        recordText.text = recordInt.ToString();
        groundGenerator.BaseGenerator();
        for (int i = 0; i < levelCount; i++)
        {
           groundGenerator.RandomGenerator();
        }
    }

    void CheckGodMode()
    {
        string str = PlayerPrefs.GetString("GodMode");
        if (str == "On")
            player.tag = "God";
        if (str == "Off")
            player.tag = "Player";
    }

    void CheckMusic()
    {
        string str = PlayerPrefs.GetString("Toggle");
        if (str == "On")
            Mixer.audioMixer.SetFloat("MasterVolume", 0f);
        if (str == "Off")
            Mixer.audioMixer.SetFloat("MasterVolume", -80f);
    }

    public void UpdateCoinCount(int value)
    {
        Debug.Log("Player picked up another coin for " + value);
        currentCoins += value;
        PlayerPrefs.SetInt("Coin", currentCoins);
        PlayerPrefs.Save();
        coin.text = currentCoins.ToString();
    }

    public void UpdateDistanceCount()
    {
        if (player.transform.position.z > currentDistance)
        {
            currentDistance += 1;
            if (groundGenerator.CheckGroundList()) { groundGenerator.RandomGenerator(); }
            distance.text = currentDistance.ToString();
            CheckRecord();
        }
    }

    void CheckRecord()
    {
        if( currentDistance > recordInt )
        {
            PlayerPrefs.SetInt("Record", currentDistance);
            PlayerPrefs.Save();
            recordInt = currentDistance;
            recordText.text = recordInt.ToString();
        }
    }

    public bool CanPlay()
    {
        return canPlay;
    }
    public bool CanFollow()
    {
        return canFollow;
    }

    public void StartPlay()
    {
        canPlay = true;
    }
    public void StartFollow()
    {
        canFollow = true;
    }

    public void Shoot()
    {
        ballGameObject = EasyObjectPool.instance.GetObjectFromPool("Ball", player.transform.position + new Vector3(0, 0.8f, 0), Quaternion.identity);
        rotation = player.transform.eulerAngles.y;
        direction = ballGameObject.transform.position;
        SetDirection();
        ballGameObject.transform.DOMove(direction, 0.1f).OnComplete(EndMove);
    }
    public void GameOver()
    {
        camera.GetComponent<FollowingCamera>().enabled = false;
    }

    void SetDirection()
    {
        if (rotation >= 0 && rotation < 90)
        {
            direction  += Vector3.forward;
        }
        if (rotation >= 90 && rotation < 180)
        {
            direction += Vector3.right;
        }
        if (rotation >= 180 && rotation < 270)
        {
            direction -= Vector3.forward;
        }
        if (rotation >= 270)
        {
            direction -= Vector3.right;
        }
    }
    void EndMove()
    {
        EasyObjectPool.instance.ReturnObjectToPool(ballGameObject);
    }
}
