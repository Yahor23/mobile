﻿using MarchingBytes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGenerator : MonoBehaviour
{
    public List<GameObject> platform = new List<GameObject>();
    public List<float> height = new List<float>();
    public List<string> pool = new List<string>();
    private Queue<GameObject> ground = new Queue<GameObject>();
    public int maxCount = 25;

    private float offset = 0;
    private int rndRange = 0;
    private float endBasePos = -2;
    private float startBasePos = -7;
    private float lastPos = 0;
    private float lastScale = 0;
    private Vector3 pos;
    private float scaleObject;

    public void BaseGenerator()
    {
        offset = startBasePos;
        pos = new Vector3(0, height[0], offset);
        for (float i = startBasePos; i < endBasePos; ++i)
        {
            GameObject go = EasyObjectPool.instance.GetObjectFromPool(pool[0],pos,Quaternion.identity);
            ground.Enqueue(go);
            startBasePos = go.transform.position.z;
            lastScale = go.transform.localScale.z;
            offset = startBasePos + lastScale;
            pos.z = offset;
            go.transform.parent = this.transform;
        }
        pos = new Vector3(0, height[0], offset);
        for (float i = endBasePos; i <= lastPos; ++i)
        {
            GameObject go = EasyObjectPool.instance.GetObjectFromPool(pool[10], pos, Quaternion.identity);
            ground.Enqueue(go);
            endBasePos = go.transform.position.z;
            lastScale = go.transform.localScale.z;
            offset = endBasePos + lastScale;
            pos.z = offset;
            go.transform.parent = this.transform;
        }
    }
    public void RandomGenerator()
    {

        rndRange = Random.Range(1, platform.Count);
        CreateLevelObject(height[rndRange], rndRange);
    }

    public bool CheckGroundList()
    {
        if(ground.Count >= maxCount)
        {
            GameObject go = ground.Peek();
            if (scaleObject == 0) { scaleObject = go.transform.localScale.z; }
            if (scaleObject == 1)
            {
                EasyObjectPool.instance.ReturnObjectToPool(ground.Dequeue());
                scaleObject = 0;
                return true; 
            }
            else { --scaleObject; return false; }
        }
        return true;
    }
    public void CreateLevelObject(float height, int value)
    {
        pos.y = height;
        GameObject go = EasyObjectPool.instance.GetObjectFromPool(pool[rndRange], pos, Quaternion.identity);
        ground.Enqueue(go);
        float offset = lastPos + (lastScale * 0.5f);
        offset += (go.transform.localScale.z) * 0.5f;

        pos.z = offset;

        go.transform.position = pos;

        lastPos = go.transform.position.z;
        lastScale = go.transform.localScale.z;

        go.transform.parent = this.transform;
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
