﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settings : MonoBehaviour
    {
        public AudioMixerGroup Mixer;
        Toggle Tgl;
        Toggle TglGodMode;

    void Start()
        {
        Tgl = GameObject.Find("Music Toggle").GetComponent<Toggle>();
        TglGodMode = GameObject.Find("God Mode").GetComponent<Toggle>();

        string str = PlayerPrefs.GetString("Toggle");
        if (str == "On")
                Tgl.isOn = true;
            if (str == "Off")
                Tgl.isOn = false;

        str = PlayerPrefs.GetString("GodMode");
        if (str == "On")
            TglGodMode.isOn = true;
        if (str == "Off")
            TglGodMode.isOn = false;

    }

        public void ToggleMusic(bool enabled)
        {
            if (enabled)
            {
                Mixer.audioMixer.SetFloat("MasterVolume", 0f);
                PlayerPrefs.SetString("Toggle", "On");
                PlayerPrefs.Save();
            }
            else
            {
                Mixer.audioMixer.SetFloat("MasterVolume", -80f);
                PlayerPrefs.SetString("Toggle", "Off");
                PlayerPrefs.Save();
            }
        }

        public void Back()
        {
            ManagerUI.instance.Back();
        }
        
        public void SetGodMode(bool enabled)
        {
        if (enabled)
        {
            PlayerPrefs.SetString("GodMode", "On");
            PlayerPrefs.Save();
        }
            else
        {
            PlayerPrefs.SetString("GodMode", "Off");
            PlayerPrefs.Save();
        }
        }
    }
