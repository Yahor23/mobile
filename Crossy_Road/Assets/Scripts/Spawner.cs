﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;
using DG.Tweening;

public class Spawner : MonoBehaviour
{
    public Transform startPos = null;
    public GameObject player = null;
    public SpawnController spawnController = null;

    public float delayMin = 1.5f;
    public float delayMax = 5f;
    public float speedMin = 1f;
    public float speedMax = 4f;
    public float limit = 5f;

    public int distanceSpawn = 18;

    public bool stopSpawn = false;
    private bool isChecked = false;
    private float lastTime = 0;
    private float delayTime = 0;
    private float speed = 0;
    private Queue<GameObject> items = new Queue<GameObject>();
    [HideInInspector] public string poolName;
    [HideInInspector] public bool goLeft = false;
    [HideInInspector] public Vector3 spawnLeftPos;
    [HideInInspector] public float spawnRightPos = 0;

    void Start()
    {
        player = GameObject.Find("Player");
        spawnLeftPos.x = Mathf.Round(spawnLeftPos.x);
        speed = Random.Range(speedMin, speedMax);
    }

    void Update()
    {
        StopSpawn();
        if (!isChecked) { CheckPosition(); }
        else return;
        if (stopSpawn) { return; }
        if (Time.time > lastTime + delayTime)
        {
            lastTime = Time.time;
            delayTime = Random.Range(delayMin, delayMax);
            SpawnItem();
        }
    }

   
    void CheckPosition()
    {
        float difference = player.transform.position.z - spawnLeftPos.z;
        if (difference > limit)
        {
            while (items.Count != 0)
            {
                EasyObjectPool.instance.ReturnObjectToPool(items.Dequeue());
            }
            isChecked = true;
        }
        if (items.Count != 0)
        {
            if (items.Peek().transform.position.x > distanceSpawn / 2 + 2 || items.Peek().transform.position.x < -distanceSpawn / 2 - 2)
            {
                GameObject go = items.Dequeue();
                for (int i = 0; i < go.transform.childCount; ++i)
                {
                    if (go.transform.GetChild(i).tag == player.tag)
                        player.transform.parent = null;
                }               
                EasyObjectPool.instance.ReturnObjectToPool(go);
            }
        }
    }
    void StopSpawn()
    {
        float difference = player.transform.position.z - spawnLeftPos.z + 2;
        if (difference > limit) stopSpawn = true;
    }
    void SpawnItem()
    {
        float rotate = 0;
        if (goLeft)
            rotate = 180;
        GameObject go = EasyObjectPool.instance.GetObjectFromPool(poolName, spawnLeftPos, Quaternion.Euler(0,rotate,0));
        go.GetComponent<Mover>().speed = speed;
        items.Enqueue(go);
    }
 
    public void SetParameters()
    {
        spawnLeftPos.x = Mathf.Round(spawnLeftPos.x);
        speed = Random.Range(speedMin, speedMax);
        isChecked = false;
        stopSpawn = false;
    }
}

