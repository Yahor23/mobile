﻿using UnityEngine;
using DG.Tweening;
using MarchingBytes;
public class Mover : MonoBehaviour
{
    public float direction = 1;
    public float endPoint = 30f;
    public float speed = 1.0f;
    public float moveDirection = 0;
    public bool parentOnTrigger = true;
    public bool hitBoxOnTrigger = false;
    public GameObject moverObject = null;

    private bool isMoving = false;


    void Update()
    {
            this.transform.Translate(speed * Time.deltaTime, 0, 0);
    }

    private void Moving()
    {
        transform.DOMoveX(direction*10, 10f).OnComplete(ReturnToPool);
        isMoving = true;

    }

    void ReturnToPool()
    {
        isMoving = false;
        EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball" && gameObject.tag != "Log") 
        {
            EasyObjectPool.instance.ReturnObjectToPool(gameObject);
            return;
        }

        if (other.tag == "Player" || other.tag == "God")
        {
            if (parentOnTrigger)
            {
                other.transform.parent = this.transform;
            }

            if (hitBoxOnTrigger && other.tag == "Player")
            {
                other.GetComponent<PlayerController>().GetHit();
            }

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" || other.tag == "God")
        {
            if (parentOnTrigger)
            {
                other.transform.parent = null;
            }
        }
    }
}
