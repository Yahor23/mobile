﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScreen : MonoBehaviour
{
   public GameObject Start;
   public void StartPlay()
    {
        Manager.instance.StartPlay();
    }
    public void StartFollow()
    {
        Manager.instance.StartFollow();
    }

    public void DestroyStartScreen()
    {
        Destroy(Start);
    }
}
