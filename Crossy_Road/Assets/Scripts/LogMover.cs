﻿using UnityEngine;
using DG.Tweening;
using MarchingBytes;
public class LogMover : MonoBehaviour
{
    public float direction = 1;
    public float endPoint = 30f;
    public float speed = 2.0f;
    public float moveDirection = 0;
    public bool parentOnTrigger = true;
    public bool hitBoxOnTrigger = false;
    public GameObject moverObject = null;

    private bool isMoving = false;


    void Update()
    {
        //this.transform.Translate(speed * Time.deltaTime, 0, 0);
        if(!isMoving)
            Moving();
    }

    private void Moving()
    {
        transform.DOMoveX(direction * speed, 10f).OnComplete(ReturnToPool);
        isMoving = true;

    }

    void ReturnToPool()
    {
        isMoving = false;
        EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (parentOnTrigger)
            {
                other.transform.parent = this.transform;
            }

            if (hitBoxOnTrigger)
            {
                other.GetComponent<PlayerController>().GetHit();
            }

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (parentOnTrigger)
            {
                other.transform.parent = null;
            }
        }
    }
}
