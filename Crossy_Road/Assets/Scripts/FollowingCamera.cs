﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FollowingCamera : MonoBehaviour
{
    public bool autoMove = true;
    public GameObject player = null;
    public float speed = 0.05f;
    private bool isFollow = false;
    Tweener tweenX;
    Tweener tweenZ;
    Tweener tween;

    public Vector3 offset = new Vector3(5, 7, -4);
    Vector3 autoMoveVector = Vector3.zero;
    Vector3 pos = Vector3.zero;

    void Start()
    {
        ChangePosition();
        tween = transform.DOMove(pos, 2f).SetAutoKill(false);
    }
    void Update()
    {
        if (!Manager.instance.CanFollow()) return;
        ChangePosition();
        if (transform.position.z - player.transform.position.z < -4.5)
        {
            tween.ChangeEndValue(pos, 2f, true).Restart();
        }
        else if (transform.position.x - player.transform.position.x > 1.25f || transform.position.x - player.transform.position.x < 0.75f)
        {
            if (autoMoveVector.z > pos.z)
                pos.z = autoMoveVector.z;
            tween.ChangeEndValue(pos, 1.5f, true).Restart();
        }
        else
        {
            Debug.Log("There");
            autoMoveVector = transform.position;
            autoMoveVector.z += 0.5f;
            tween.ChangeEndValue(autoMoveVector, 3f, true).Restart();
        }
    }

    void ChangePosition()
    {
        pos = player.transform.position;
        pos.x += offset.x;
        pos.y = offset.y;
        pos.z += offset.z;
    }
}
