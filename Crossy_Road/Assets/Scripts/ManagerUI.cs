﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerUI : MonoBehaviour
{
    public GameObject player;
    public Transform ParentCanvas;
    public GameObject Shoot;
    public GameObject Distance;
    public GameObject Record;
    public GameObject RecordText;
    public GameObject StartScreen;
    public GameObject GameOver;
    public GameObject Coin;
    public GameObject CoinImage;
    public GameObject Panel;
    public GameObject CreatedPanel;
    private static ManagerUI s_Instance;
    public static ManagerUI instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = FindObjectOfType(typeof(ManagerUI)) as ManagerUI;
            }

            return s_Instance;
        }
    }

    void Awake()
    {
        Instantiate(StartScreen, ParentCanvas);
        Manager.instance.distance = Instantiate(Distance, ParentCanvas).GetComponent<Text>();
        Instantiate(RecordText, ParentCanvas);
        Manager.instance.recordText = Instantiate(Record, ParentCanvas).GetComponent<Text>();
        Manager.instance.coin = Instantiate(Coin, ParentCanvas).GetComponent<Text>();
        Instantiate(CoinImage, ParentCanvas);
        Instantiate(Shoot, ParentCanvas);
    }
    public void GuiGameOver()
    {
        Instantiate(GameOver, ParentCanvas);
    }

    public void CreatePanel()
    {
        CreatedPanel = Instantiate(Panel, ParentCanvas);
    }

    public void Back()
    {
        Destroy(CreatedPanel);
    }

    //public void SetGodMode(bool enabled)
    //{
    //    if (enabled)
    //        player.tag = "God";
    //    else
    //        player.tag = "Player";
    //}

}
