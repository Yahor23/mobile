﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class StaticTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            EasyObjectPool.instance.ReturnObjectToPool(gameObject);
        }
    }
}
