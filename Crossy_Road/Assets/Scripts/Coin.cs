﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class Coin : MonoBehaviour
{
    public int value = 1;

   
    void OnTriggerEnter (Collider collider)
    {
        if (collider.tag == "Player" || collider.tag == "God")
        {
            Manager.instance.UpdateCoinCount(value);
            EasyObjectPool.instance.ReturnObjectToPool(gameObject);
        }
    }
}
