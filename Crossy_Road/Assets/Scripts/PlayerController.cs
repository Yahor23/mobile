﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Text;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    public AudioSource moveAudio;
    public AudioSource deadAudio;
    public AudioSource deadByWaterAudio;
    public AudioSource coinAudio;

    public float moveDistance = 1;
    public float moveTime = 0.4f;
    public float rotateSpeed = 0.2f;
    public float colliderDistCheck = 1.5f;
    public float leftBorder = -5f;
    public float rightBorder = 4f;

    public bool isGameOver = false;
    public bool isIdle = true;
    public bool isDead = false;
    public bool isMoving = false;
    public bool isJumping = false;
    public bool jumpStart = false;
    private bool isMoved = false;
    public ParticleSystem particle = null;
    public GameObject chick = null;

    private Renderer renderer = null;
    private bool isVisible = false;
    private Vector3 checkPos;
    private Vector2 startPos;
    private Vector2 endPos;
    private Touch touch;

    void Start()
    {
        isGameOver = false;
        renderer = chick.GetComponent<Renderer>();
    }

    void Update()
    {
        if (!Manager.instance.CanPlay()) return;
        if (isDead) return;
        CheckPosition();
        CanIdle();
        CanMove();
        IsVisible();
    }

    void CanIdle()
    {
        if (isIdle)
        {
            if (Input.touchCount > 0)
            {
                touch = Input.GetTouch(0);
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId) || EventSystem.current.currentSelectedGameObject != null)
                    return;
                switch (touch.phase)
                {

                    case TouchPhase.Began:

                        startPos = touch.position;
                        jumpStart = true;
                        break;

                    case TouchPhase.Moved:
                        endPos = touch.position - startPos;
                        isMoved = true;
                        break;

                    case TouchPhase.Ended:
                        if (isMoved)
                            SetDirection();
                        else
                            checkPos = Vector3.forward;
                        if (!CheckCanMove())
                            jumpStart = false;
                        isMoved = false;
                        break;
                }
            }
        }
    }

    void SetDirection()
    {
        if (endPos.y >= 0 && endPos.y >= Mathf.Abs(endPos.x))
            checkPos = Vector3.forward;
        if (endPos.y < 0 && Mathf.Abs(endPos.y) >= Mathf.Abs(endPos.x))
            checkPos = -Vector3.forward;
        if (endPos.x > 0 && endPos.x > Mathf.Abs(endPos.y))
            checkPos = Vector3.right;
        if (endPos.x < 0 && Mathf.Abs(endPos.x) > Mathf.Abs(endPos.y))
            checkPos = -Vector3.right;
    }
    bool CheckCanMove()
    {
        Physics.Raycast(this.transform.position, checkPos, out RaycastHit hit, colliderDistCheck);
        Debug.DrawRay(this.transform.position, checkPos * colliderDistCheck * 20, Color.red, 2);

        if (hit.collider == null || (hit.collider != null && hit.collider.tag != "Collider"))
        {
            if (CheckBorders())
            {
                SetMove();
                return true;
            }
        }
        return false;
    }

    bool CheckBorders()
    {
        if(checkPos == -Vector3.right)
        {
            if (transform.position.x <= leftBorder)
                return false;
        }
        if (checkPos == Vector3.right)
        {
            if (transform.position.x >= rightBorder)
                return false;
        }
        return true;
    }
    void CheckIfCanMove()
    {
        Physics.Raycast(this.transform.position, checkPos, out RaycastHit hit, colliderDistCheck);
        Debug.DrawRay(this.transform.position, checkPos * colliderDistCheck * 20, Color.red, 2);

        if (hit.collider == null || (hit.collider != null && hit.collider.tag != "Collider"))
        {
            SetMove();
        }
    }

    void SetMove()
    {
        isIdle = false;
        isMoving = true;
        jumpStart = true;
    }

    void CanMove()
    {
        if (isMoving)
        {
            if(checkPos == Vector3.forward)
            {
                Moving(new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDistance));
                transform.DORotateQuaternion(Quaternion.Euler(0, 0, 0), rotateSpeed);
            }
            else if (checkPos == -Vector3.forward)
            {
                Moving(new Vector3(transform.position.x, transform.position.y, transform.position.z - moveDistance));
                transform.DORotateQuaternion(Quaternion.Euler(0, 180, 0), rotateSpeed);
            }
            else if (checkPos == -Vector3.right)
            {
                Moving(new Vector3(transform.position.x - moveDistance, transform.position.y, transform.position.z));
                transform.DORotateQuaternion(Quaternion.Euler(0, -90, 0), rotateSpeed);
            }
            else if (checkPos == Vector3.right)
            {
                Moving(new Vector3(transform.position.x + moveDistance, transform.position.y, transform.position.z));
                transform.DORotateQuaternion(Quaternion.Euler(0, 90, 0), rotateSpeed);
            }
        }
    }

    void Moving(Vector3 pos)
    {
        moveAudio.pitch = Random.Range(0.9f, 1.1f);
        moveAudio.Play();
        isIdle = false;
        isMoving = false;
        isJumping = true;
        jumpStart = false;
        pos.x = Mathf.Round(pos.x);
        transform.DOMove(pos, moveTime).OnComplete(MoveComplete);
    }

    void MoveComplete()
    {
        isIdle = true;
        isMoving = false;
        isJumping = false;
        jumpStart = false;
        SetMoveForwardState();
        CheckWater();
    }
    void CheckWater()
    {
        Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, colliderDistCheck);
        Debug.DrawRay(this.transform.position, checkPos * colliderDistCheck * 20, Color.red, 2);
        if (hit.collider.tag == "Dead" && transform.parent == null && gameObject.tag == "Player")
            GetHitByWater();
    }

    void SetMoveForwardState()
    {
        Manager.instance.UpdateDistanceCount();
    }

    void CheckPosition()
    {
        if (( transform.position.x < leftBorder || transform.position.x > rightBorder) && gameObject.tag != "God")
        {
            gameObject.transform.parent = null;
            GetHitByWater();
        }
    }

    void IsVisible()
    {
        if (renderer.isVisible) isVisible = true;
        if (!renderer.isVisible && isVisible == true)
        {
            GetHit();
        }
    }

    public void GetHit()
    {
        if (!isGameOver)
        {
            Manager.instance.GameOver();
            ManagerUI.instance.GuiGameOver();
            isDead = true;
            deadAudio.Play();
            isGameOver = true;
        }
    }

    public void GetHitByWater()
    {
        if (!isGameOver)
        {
            deadByWaterAudio.Play();
            Manager.instance.GameOver();
            ManagerUI.instance.GuiGameOver();
            isDead = true;
            transform.position -= new Vector3(0, 5, 0);
            particle.Play();
            isGameOver = true;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Coin")
            coinAudio.Play();
    }

}
