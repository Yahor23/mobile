﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimatorController : MonoBehaviour
{
    public float rotateSpeed = 0.2f;
    public PlayerController playerController = null;
    private Animator animator = null;


    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    void Update()
    {
        if (playerController.isDead)
        {
            animator.SetBool("isDead", true);
        }

        if (playerController.jumpStart)
        {
            animator.SetBool("isStartJumping", true);
        }
        else if (playerController.isJumping)
        {
            animator.SetBool("isJumping", true);
        }
        else
        {
            animator.SetBool("isStartJumping", false);
            animator.SetBool("isJumping", false);
        }

    }
}
